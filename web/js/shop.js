$(function(){

    $("#schedule-container").on("change", '.close-schedule', function(){
        if (this.checked)
            $($(this).parents(".schedule-body")[0]).find(".shop-time").hide().find('input').val("");
        else
            $($(this).parents(".schedule-body")[0]).find(".shop-time").show();
    });

    $(".close-schedule").trigger("change");

    $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
        var datePickers = $(this).find('[data-krajee-kvdatepicker]');
        datePickers.each(function(index, el) {
            $(this).kvDatepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        });
    });

});