<?php

use yii\db\Migration;

/**
 * Handles the creation of table `schedule_by_week`.
 */
class m180319_102754_create_schedule_by_week_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('schedule_by_week', [
            'id' => $this->primaryKey(),
            'shop_id'=>$this->integer()->notNull(),
            'day_of_week'=>$this->tinyInteger(1)->notNull(),
            'open_time'=>$this->time()->notNull(),
            'close_time'=>$this->time()->notNull()
        ]);
        $this->createIndex('day_of_week', 'schedule_by_week', [
            'shop_id',
            'day_of_week'
        ],true);
        $this->addForeignKey(
            'schedule_by_week_ibfk_1',
            'schedule_by_week',
            'shop_id',
            'shop',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('schedule_by_week_ibfk_1', 'schedule_by_week');
        $this->dropIndex('day_of_week', 'schedule_by_week');
        $this->dropTable('schedule_by_week');
    }
}
