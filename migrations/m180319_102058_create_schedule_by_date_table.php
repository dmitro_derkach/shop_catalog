<?php

use yii\db\Migration;

/**
 * Handles the creation of table `schedule_by_date`.
 */
class m180319_102058_create_schedule_by_date_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('schedule_by_date', [
            'id' => $this->primaryKey(),
            'shop_id'=>$this->integer()->notNull(),
            'date'=>$this->date()->notNull(),
            'open_time'=>$this->time()->null(),
            'close_time'=>$this->time()->null()
        ]);
        $this->createIndex('date', 'schedule_by_date', [
            'shop_id',
            'date'
        ],true);
        $this->addForeignKey(
            'schedule_by_date_ibfk_1',
            'schedule_by_date',
            'shop_id',
            'shop',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('schedule_by_date_ibfk_1', 'schedule_by_date');
        $this->dropIndex('date', 'schedule_by_date');
        $this->dropTable('schedule_by_date');
    }
}
