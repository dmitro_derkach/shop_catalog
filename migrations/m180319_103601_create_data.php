<?php

use yii\db\Migration;

/**
 * Class m180319_103601_create_data
 */
class m180319_103601_create_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('shop', [
            'name'=>'shop 1',
            'description'=>'',
        ]);
        $this->insert('schedule_by_week', [
            'shop_id'=>'1',
            'day_of_week'=>'0',
            'open_time'=>'09:00',
            'close_time'=>'19:00'
        ]);
        $this->insert('schedule_by_week', [
            'shop_id'=>'1',
            'day_of_week'=>'1',
            'open_time'=>'09:00',
            'close_time'=>'19:00'
        ]);
        $this->insert('schedule_by_week', [
            'shop_id'=>'1',
            'day_of_week'=>'2',
            'open_time'=>'09:00',
            'close_time'=>'19:00'
        ]);
        $this->insert('schedule_by_week', [
            'shop_id'=>'1',
            'day_of_week'=>'3',
            'open_time'=>'09:00',
            'close_time'=>'19:00'
        ]);
        $this->insert('schedule_by_week', [
            'shop_id'=>'1',
            'day_of_week'=>'4',
            'open_time'=>'09:00',
            'close_time'=>'19:00'
        ]);
        $this->insert('schedule_by_week', [
            'shop_id'=>'1',
            'day_of_week'=>'5',
            'open_time'=>'10:00',
            'close_time'=>'16:00'
        ]);
        $this->insert('schedule_by_date', [
            'shop_id'=>'1',
            'date'=>'2018-03-25',
            'open_time'=>'10:00',
            'close_time'=>'12:00'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180319_103601_create_data cannot be reverted.\n";

        return false;
    }
}
