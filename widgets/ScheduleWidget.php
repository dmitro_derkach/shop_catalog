<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 19.03.2018
 * Time: 1:26
 */

namespace app\widgets;


use yii\base\Widget;

class ScheduleWidget extends Widget
{

    public $schedule = [];

    public function run()
    {
        return $this->render('schedule', ['schedule'=>$this->schedule]);
    }

}