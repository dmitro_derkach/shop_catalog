<?php foreach ($schedule as $data) : ?>
    <div>
        <?=$data['day']?>:
        <?php if ($data['open_time'] && $data['close_time']) : ?>
            <?= $data['open_time']?> - <?= $data['close_time']?>
        <?php else: ?>
            closed
        <?php endif; ?>
    </div>
<?php endforeach; ?>
