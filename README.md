Shop catalog project
==============================
#### PHP: 5.5.*+
#### Framework: Yii 2.0.14
#### Database: MariaDB 10+ or MySQL 5.6+

INSTALLATION
------------
#### Create database shop_catalog

#### Setting db config (config/db.php)

#### Run composer
~~~
php composer.phar update
~~~

#### Run migrations
~~~
./yii migrate
~~~