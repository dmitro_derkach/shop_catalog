<?php

namespace app\controllers;

use app\base\Model;
use app\models\Shop;
use Yii;
use app\models\ScheduleByDate;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ScheduleController implements the CRUD actions for ScheduleByDate model.
 */
class ScheduleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionCreate($id)
    {
        $modelShop = $this->findShopModel($id);

        $schedulesByDate = Model::createMultiple(ScheduleByDate::class, [], 'shop_id', $modelShop->id);
        if (Model::loadMultiple($schedulesByDate, Yii::$app->request->post())) {
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validateMultiple($schedulesByDate);
            }

            if (Model::validateMultiple($schedulesByDate)) {
                if (Model::saveMultiple($modelShop, $schedulesByDate))
                    return $this->redirect(['/shop/view', 'id' => $modelShop->id]);
            }
        }

        return $this->render('create', [
            'models' => (empty($schedulesByDate)) ? [new ScheduleByDate] : $schedulesByDate,
            'modelShop'=>$modelShop,
        ]);
    }

    /**
     * Updates an existing ScheduleByDate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			// ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
			if ($model->save())
				return $this->redirect(['/shop/view', 'id' => $model->shop_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $shop_id = $model->shop_id;
        $model->delete();
        return $this->redirect(['/shop/view', 'id'=>$shop_id]);
    }

    /**
     * Finds the ScheduleByDate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduleByDate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScheduleByDate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     * @return Shop
     * @throws NotFoundHttpException
     */
    protected function findShopModel($id)
    {
        if (($model = Shop::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
