<?php

namespace app\controllers;

use app\base\Model;
use app\models\ScheduleByDateSearch;
use app\models\ScheduleByWeek;
use Yii;
use app\models\Shop;
use app\models\ShopSearch;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * ShopController implements the CRUD actions for Shop model.
 */
class ShopController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Shop models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date'=>$searchModel->schedule_search,
        ]);
    }

    /**
     * Displays a single Shop model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new ScheduleByDateSearch($id);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('view', [
            'scheduleSearchModel' => $searchModel,
            'scheduleDataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Shop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return array|string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $modelShop = new Shop;
        if ($modelShop->load(Yii::$app->request->post())) {

            $schedulesByWeek = ScheduleByWeek::createMultiple(ScheduleByWeek::class);
            Model::loadMultiple($schedulesByWeek, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($schedulesByWeek),
                    ActiveForm::validate($modelShop)
                );
            }

            // validate all models
            $valid = $modelShop->validate();
            $valid = Model::validateMultiple($schedulesByWeek) && $valid;
            if ($valid) {
                if (ScheduleByWeek::saveMultiple($modelShop, $schedulesByWeek, 'shop_id'))
                    return $this->redirect(['view', 'id' => $modelShop->id]);
            }
        }

        return $this->render('create', [
            'daysOfWeek'=>ScheduleByWeek::daysOfWeek(),
            'modelShop' => $modelShop,
            'schedulesByWeek' => (empty($schedulesByWeek)) ? ScheduleByWeek::createModels() : $schedulesByWeek
        ]);
    }

    /**
     * Updates an existing Shop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $modelShop = $this->findModel($id);
        $schedulesByWeek = $modelShop->schedulesByWeek;
        $schedulesByWeek = ScheduleByWeek::createModels($schedulesByWeek, 'update');

        if ($modelShop->load(Yii::$app->request->post())) {

            $schedulesByWeek = ScheduleByWeek::createMultiple(ScheduleByWeek::class, $schedulesByWeek, 'shop_id', $modelShop->id);
            Model::loadMultiple($schedulesByWeek, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($schedulesByWeek),
                    ActiveForm::validate($modelShop)
                );
            }

            // validate all models
            $valid = $modelShop->validate();
            $valid = Model::validateMultiple($schedulesByWeek) && $valid;

            if ($valid) {
                if (ScheduleByWeek::saveMultiple($modelShop, $schedulesByWeek, null, 'update',
                    ScheduleByWeek::class))
                    return $this->redirect(['view', 'id' => $modelShop->id]);
            }
        }

        return $this->render('update', [
            'modelShop' => $modelShop,
            'schedulesByWeek' => (empty($schedulesByWeek)) ? [new ScheduleByWeek] : $schedulesByWeek,
            'daysOfWeek'=>ScheduleByWeek::daysOfWeek(),
        ]);

    }

    /**
     * Deletes an existing Shop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shop::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
