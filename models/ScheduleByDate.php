<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedule_by_date".
 *
 * @property int $id
 * @property int $shop_id
 * @property string $date
 * @property string $open_time
 * @property string $close_time
 * @property bool isClosed
 * @property Shop $shop
 */
class ScheduleByDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule_by_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['shop_id'], 'integer'],
            [['date'], 'date', 'format'=>'yyyy-MM-dd'],
            [['open_time', 'close_time'], 'time', 'format'=>'HH:mm'],
            [['shop_id', 'date'], 'unique', 'targetAttribute' => ['shop_id', 'date']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shop_id' => Yii::t('app', 'Shop ID'),
            'date' => Yii::t('app', 'Date'),
            'open_time' => Yii::t('app', 'Open Time'),
            'close_time' => Yii::t('app', 'Close Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

    /**
     * @return bool
     */
    public function getIsClosed(){
        if ($this->isNewRecord)
            return false;
        else
            return !($this->open_time && $this->close_time);
    }
}
