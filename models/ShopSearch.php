<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Shop;

/**
 * ShopSearch represents the model behind the search form of `app\models\Shop`.
 */
class ShopSearch extends Shop
{

    public $schedule_search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            [['schedule_search'], 'date', 'format'=>'yyyy-MM-dd']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $this->schedule_search = null;
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'name', $this->name]);
        if ($this->schedule_search){
            $query->joinWith(['schedulesByDate date'=>function($q){
                $q->onCondition(['date.date'=>$this->schedule_search]);
            }, 'schedulesByWeek week'])
                ->where("(date.id IS NOT NULL AND date.open_time IS NOT NULL AND date.close_time IS NOT NULL)
                OR (date.id IS NULL 
                AND week.day_of_week = IF(DAYOFWEEK(:date) = 1, 6, DAYOFWEEK(:date) - 2))", [
                    ':date'=>$this->schedule_search,
                ]);
        }

        return $dataProvider;
    }
}
