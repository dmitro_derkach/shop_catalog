<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScheduleByDate;

/**
 * ScheduleByDateSearch represents the model behind the search form of `app\models\ScheduleByDate`.
 */
class ScheduleByDateSearch extends ScheduleByDate
{

    /**
     * ScheduleByDateSearch constructor.
     * @param $id
     * @param array $config
     */
    public function __construct($id, array $config = [])
    {
        parent::__construct($config);
        $this->shop_id = $id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'open_time', 'close_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScheduleByDate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>[
                'defaultOrder'=>[
                    'date'=>SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'shop_id' => $this->shop_id,
            'date' => $this->date,
            'open_time' => $this->open_time,
            'close_time' => $this->close_time,
        ]);

        return $dataProvider;
    }
}
