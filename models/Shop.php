<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shop".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property ScheduleByDate[] $schedulesByDate
 * @property ScheduleByWeek[] $schedulesByWeek
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedulesByDate()
    {
        return $this->hasMany(ScheduleByDate::className(), ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedulesByWeek()
    {
        return $this->hasMany(ScheduleByWeek::className(), ['shop_id' => 'id']);
    }

    /**
     * @param string $date
     * @return array
     */
    public function getScheduleByDate($date){
        $scheduleByDate = ScheduleByDate::find()->where([
            'shop_id'=>$this->id,
            'date'=>$date,
        ])->one();
        if ($scheduleByDate){
            return [
                'open_time'=>$scheduleByDate->open_time?Yii::$app->formatter->asTime($scheduleByDate->open_time):null,
                'close_time'=>$scheduleByDate->close_time?Yii::$app->formatter->asTime($scheduleByDate->close_time):null,
            ];
        }
        $scheduleByWeek = ScheduleByWeek::find()->where([
            'shop_id'=>$this->id
        ])->andWhere("day_of_week = IF(DAYOFWEEK(:date) = 1, 6, DAYOFWEEK(:date) - 2)", [":date"=>$date])
        ->one();
        if ($scheduleByWeek){
            return [
                'open_time'=>Yii::$app->formatter->asTime($scheduleByWeek->open_time),
                'close_time'=>Yii::$app->formatter->asTime($scheduleByWeek->close_time),
            ];
        }
        return [
            'open_time'=>null,
            'close_time'=>null
        ];
    }


    public function getSchedule($date = null)
    {
        $chedulesByWeek = $this->schedulesByWeek;
        $days = ScheduleByWeek::daysOfWeek();
        $schedule = [];
        foreach ($chedulesByWeek as $model){
            $schedule[$model->day_of_week] = [
                'day'=>$days[$model->day_of_week],
                'open_time' => Yii::$app->formatter->asTime($model->open_time),
                'close_time' => Yii::$app->formatter->asTime($model->close_time),
            ];
        }
        foreach ($days as $key=>$day){
            if (!array_key_exists($key, $schedule)){
                $schedule[$key] = [
                    'day'=>$day,
                    'open_time' => null,
                    'close_time' => null,
                ];
            }
        }
        ksort($schedule);
        $today = $this->getScheduleByDate(date("Y-m-d"));
        if ($date){
            $schedule_by_date = $this->getScheduleByDate($date);
            $schedule_by_date = [
                "$date"=>[
                    'day'=>"$date",
                    'open_time'=>$schedule_by_date['open_time'],
                    'close_time'=>$schedule_by_date['close_time'],
                ]
            ];
        }
        $today = [
            'today'=>[
                'day'=>'today',
                'open_time'=>$today['open_time'],
                'close_time'=>$today['close_time'],
            ],
        ];

        $schedule = $today + $schedule;

        if ($date)
            $schedule = $schedule_by_date + $schedule;

        return $schedule;
    }
}
