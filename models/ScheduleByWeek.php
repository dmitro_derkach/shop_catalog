<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "schedule_by_week".
 *
 * @property int $id
 * @property int $shop_id
 * @property int $day_of_week
 * @property string $open_time
 * @property string $close_time
 *
 * @property Shop $shop
 */
class ScheduleByWeek extends \yii\db\ActiveRecord
{

    public $shop_is_closed = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule_by_week';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['day_of_week'], 'required'],
            [['shop_is_closed'], 'boolean'],
            [['day_of_week'], 'in', 'range'=>[0, 1, 2, 3, 4, 5, 6]],
            [['shop_id'], 'integer'],
            [['open_time', 'close_time'], 'validateTime', 'skipOnEmpty'=>false, 'params'=>['format'=>'HH:mm']],
            [['shop_id', 'day_of_week'], 'unique', 'targetAttribute' => ['shop_id', 'day_of_week']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateTime($attribute, $params){
        if (!$this->shop_is_closed){
            if (!$this->$attribute){
                $this->addError($attribute, "Field cannot be blank.");
                return;
            }
            $validator = new \yii\validators\DateValidator(['type'=>\yii\validators\DateValidator::TYPE_TIME, 'format'=>$params['format']]);
            if (!$validator->validate($this->$attribute, $error))
                $this->addError($attribute, $error);
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shop_id' => Yii::t('app', 'Shop ID'),
            'day_of_week' => Yii::t('app', 'Day Of Week'),
            'open_time' => Yii::t('app', 'Open Time'),
            'close_time' => Yii::t('app', 'Close Time'),
            'shop_is_closed'=> Yii::t('app', 'Shop is closed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

    /**
     * @return array
     */
    public static function daysOfWeek()
    {
        return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    }

    /**
     * Creates and populates a set of models for schedule of week.
     *
     * @param string $modelClass
     * @param array $multipleModels
     * @param string $relationalColumn
     * @return array
     */
    public static function createMultiple($modelClass, $multipleModels = [], $relationalColumn = null, $relationalValue = null)
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $multipleModels = array_filter($multipleModels, function($model){
                return $model->id;
            });
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
                if ($relationalColumn && $relationalValue)
                    $models[count($models) - 1]->$relationalColumn = $relationalValue;
            }
        }

        unset($model, $formName, $post);

        return $models;
    }

    /**
     *Create schedule models for every day of week (if not exists)
     *
     * @param array $models
     * @param string $scenario
     * @return array
     */
    public static function createModels($models = [], $scenario = 'create')
    {
        $exist_days = array_map(function($data){
            return $data->day_of_week;
        }, $models);
        foreach (self::daysOfWeek() as $key=>$day){
            if (in_array($key, $exist_days))
                continue;
            $model = new self;
            $model->day_of_week = $key;
            if ($scenario == 'update')
                $model->shop_is_closed = true;
            $models[] = $model;
        }
        usort($models, function($first, $second){
            return $first->day_of_week < $second->day_of_week? -1 : 1;
        });
        return $models;
    }

    /**
     * Save or update multiple models for schedule
     *
     * @param ActiveRecord $baseModel
     * @param ActiveRecord[] $multipleModels
     * @param string $relationalColumn
     * @param string $scenario
     * @param array $oldIDs
     * @param string $multipleModelsClassName
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function saveMultiple($baseModel, $multipleModels, $relationalColumn = null, $scenario = 'create',
                                        $multipleModelsClassName = null)
    {
        if ($scenario == 'update') {
            $deletedModels = array_filter($multipleModels, function($data){
                return $data->shop_is_closed;
            });
            $deletedIDs = array_filter(ArrayHelper::map($deletedModels, 'id', 'id'));
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($flag = $baseModel->save(false)) {
                if ($scenario == 'update' && $multipleModelsClassName) {
                    if (! empty($deletedIDs)) {
                        $multipleModelsClassName::deleteAll(['id' => $deletedIDs]);
                    }
                }
                foreach ($multipleModels as $model) {
                    if ($model->shop_is_closed)
                        continue;
                    if ($relationalColumn){
                        $model->$relationalColumn = $baseModel->id;
                    }
                    if (! ($flag = $model->save())) {
                        $transaction->rollBack();
                        break;
                    }
                }
            }
            if ($flag) {
                $transaction->commit();
                return true;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }
        return false;
    }

}
