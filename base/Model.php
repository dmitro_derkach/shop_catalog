<?php

namespace app\base;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Model extends \yii\base\Model
{
    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array $multipleModels
     * @param string $relationalColumn
     * @return array
     */
    public static function createMultiple($modelClass, $multipleModels = [], $relationalColumn = null, $relationalValue = null)
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
                if ($relationalColumn && $relationalValue)
                    $models[count($models) - 1]->$relationalColumn = $relationalValue;
            }
        }

        unset($model, $formName, $post);

        return $models;
    }

    /**
     * Save or update multiple models
     *
     * @param ActiveRecord $baseModel
     * @param ActiveRecord[] $multipleModels
     * @param string $relationalColumn
     * @param string $scenario
     * @param array $oldIDs
     * @param string $multipleModelsClassName
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function saveMultiple($baseModel, $multipleModels, $relationalColumn = null, $scenario = 'create',
        $oldIDs = null, $multipleModelsClassName = null)
    {
        if ($scenario == 'update' && $oldIDs) {
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($multipleModels, 'id', 'id')));
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($flag = $baseModel->save(false)) {
                if ($scenario == 'update' && $multipleModelsClassName) {
                    if (! empty($deletedIDs)) {
                        $multipleModelsClassName::deleteAll(['id' => $deletedIDs]);
                    }
                }
                foreach ($multipleModels as $model) {
                    if ($relationalColumn){
                        $model->$relationalColumn = $baseModel->id;
                    }
                    if (! ($flag = $model->save())) {
                        $transaction->rollBack();
                        break;
                    }
                }
            }
            if ($flag) {
                $transaction->commit();
                return true;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }
        return false;
    }
}
