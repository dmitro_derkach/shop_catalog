<?php
/**
 * @var \app\models\ScheduleByDate $model
 */
?>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, isset($i)?"[{$i}]date":"date")->widget(\kartik\date\DatePicker::class, [
            'options' => [
                'placeholder' => 'Enter schedule date...',
                'autocomplete'=>'off'
            ],
            'type'=>\kartik\date\DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format'=>'yyyy-mm-dd'
            ]
        ]); ?>
    </div>
</div><!-- .row -->
<div class="row">
    <div class="col-sm-12">
        <div class="form-group field-schedulebyweek-0-shop_is_closed">
            <label><?=\yii\helpers\Html::checkbox(isset($i)?"[{$i}]shop_is_closed":"shop_is_closed", $model->isClosed, ['class'=>'close-schedule'])?> Shop is closed</label>
        </div>
    </div>
</div>
<div class="row shop-time">
    <div class="col-sm-6">
        <?= $form->field($model, isset($i)?"[{$i}]open_time":"open_time")->widget(\kartik\time\TimePicker::class, [
            'value'=>$model->open_time?Yii::$app->formatter->asTime($model->open_time):null,
            'options'=>[
                'placeholder' => 'Enter schedule time...',
                'autocomplete'=>'off',
            ],
            'pluginOptions' => [
                'showMeridian'=>false,
                'defaultTime'=>false,
            ]
        ]) ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, isset($i)?"[{$i}]close_time":"close_time")->widget(\kartik\time\TimePicker::class, [
            'value'=>$model->close_time?Yii::$app->formatter->asTime($model->close_time):null,
            'options'=>[
                'placeholder' => 'Enter schedule time...',
                'autocomplete'=>'off',
            ],
            'pluginOptions' => [
                'showMeridian'=>false,
                'defaultTime'=>false,
            ]
        ]) ?>
    </div>
</div><!-- .row -->