<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ScheduleByDate */

$this->title = Yii::t('app', 'Update Schedule By Date for {nameAttribute}', [
    'nameAttribute' => $model->shop->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $model->shop->name), 'url' => ['/shop/view', 'id'=>$model->shop_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-by-date-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id="schedule-container" class="schedule-form schedule-body">

        <?php $form = \yii\widgets\ActiveForm::begin(['enableAjaxValidation' => true]); ?>

        <?= $this->render('_form', [
            'model' => $model,
            'form'=>$form,
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>

</div>
