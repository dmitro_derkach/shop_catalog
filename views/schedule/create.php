<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model app\models\ScheduleByDate */
/* @var $modelShop app\models\Shop */

$this->title = Yii::t('app', 'Create Schedule By Date for {nameAttribute}',
    ['nameAttribute'=>Html::encode($modelShop->name)]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $modelShop->name), 'url' => ['/shop/view', 'id'=>$modelShop->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-by-date-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="customer-form">

        <?php $form = ActiveForm::begin(['id' => 'dynamic-form', 'enableAjaxValidation' => true]); ?>
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Schedules</h4></div>
            <div class="panel-body">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    //'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $models[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'date',
                        'open_time',
                        'close_time',
                    ],
                ]); ?>

                <div id="schedule-container" class="container-items"><!-- widgetContainer -->
                    <?php foreach ($models as $i => $model): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Schedule</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body schedule-body">
                                <?= $this->render('_form', [
                                    'model' => $model,
                                    'form'=>$form,
                                    'i'=>$i,
                                ]) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php DynamicFormWidget::end(); ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
