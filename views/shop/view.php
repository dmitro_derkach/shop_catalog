<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shops'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description:ntext',
            [
                'label'=>'Schedule',
                'format'=>'html',
                'value'=>function($model){
                    return \app\widgets\ScheduleWidget::widget(['schedule'=>$model->schedule]);
                }
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::begin(['id'=>'date-list']); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Add schedule'), ['schedule/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <h1>Schedule by dates:</h1>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $scheduleDataProvider,
        'filterModel' => $scheduleSearchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'date',
                'filter'=>\kartik\date\DatePicker::widget([
                    'model'=>$scheduleSearchModel,
                    'attribute'=>'date',
                    'options' => [
                        'autocomplete'=>'off'
                    ],
                    'type'=>\kartik\date\DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ])
            ],
            [
                'attribute'=>'open_time',
                'format'=>'time',
                'filter'=>\kartik\time\TimePicker::widget([
                    'model'=>$scheduleSearchModel,
                    'attribute'=>'open_time',
                    'options' => [
                        'autocomplete'=>'off'
                    ],
                    'pluginOptions' => [
                        'showMeridian'=>false,
                        'defaultTime'=>false,
                    ]
                ])
            ],
            [
                'attribute'=>'close_time',
                'format'=>'time',
                'filter'=>\kartik\time\TimePicker::widget([
                    'model'=>$scheduleSearchModel,
                    'attribute'=>'close_time',
                    'options' => [
                        'autocomplete'=>'off'
                    ],
                    'pluginOptions' => [
                        'showMeridian'=>false,
                        'defaultTime'=>false,
                    ]
                ])
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'controller'=>'schedule',
                'template'=>'{update} {delete}',
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>