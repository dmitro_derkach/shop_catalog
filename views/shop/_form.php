<?php
/**
 * @var \app\models\ScheduleByWeek[] $schedulesByWeek
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form', 'enableAjaxValidation' => true]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelShop, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelShop, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h4>Schedules by days of week</h4></div>
        <div class="panel-body">
            <div id="schedule-container" class="container-items"><!-- widgetContainer -->
                <?php foreach ($schedulesByWeek as $i => $scheduleByWeek): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left"><?=$daysOfWeek[$i]?></h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body schedule-body">
                            <?php
                            // necessary for update action.
                            if (! $scheduleByWeek->isNewRecord) {
                                echo Html::activeHiddenInput($scheduleByWeek, "[{$i}]id");
                            }
                            ?>
                            <?= Html::activeHiddenInput($scheduleByWeek, "[{$i}]day_of_week"); ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $form->field($scheduleByWeek, "[{$i}]shop_is_closed")->checkbox(['class'=>'close-schedule']) ?>
                                </div>
                            </div>
                            <div class="row shop-time">
                                <div class="col-sm-6">
                                    <?= $form->field($scheduleByWeek, "[{$i}]open_time")->widget(\kartik\time\TimePicker::class, [
                                        'value'=>$scheduleByWeek->open_time?Yii::$app->formatter->asTime($scheduleByWeek->open_time):null,
                                        'options'=>[
                                            'placeholder' => 'Enter schedule time...',
                                            'autocomplete'=>'off',
                                        ],
                                        'pluginOptions' => [
                                            'showMeridian'=>false,
                                            'defaultTime'=>false,
                                        ]
                                    ]) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($scheduleByWeek, "[{$i}]close_time")->widget(\kartik\time\TimePicker::class, [
                                        'value'=>$scheduleByWeek->close_time?Yii::$app->formatter->asTime($scheduleByWeek->close_time):null,
                                        'options'=>[
                                            'placeholder' => 'Enter schedule time...',
                                            'autocomplete'=>'off',
                                        ],
                                        'pluginOptions' => [
                                            'showMeridian'=>false,
                                            'defaultTime'=>false,
                                        ]
                                    ]) ?>
                                </div>
                            </div><!-- .row -->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($modelShop->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>