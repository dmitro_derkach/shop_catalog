<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ShopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shops');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Shop'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute'=>'description',
                'format'=>'ntext',
                'enableSorting' => false
            ],
            [
                'attribute'=>'schedule_search',
                'label'=>'Schedule',
                'format'=>'html',
                'value'=>function($model) use ($date){
                    return \app\widgets\ScheduleWidget::widget(['schedule'=>$model->getSchedule($date)]);
                },
                'filter'=>\kartik\date\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'schedule_search',
                    'options' => [
                        'autocomplete'=>'off'
                    ],
                    'type'=>\kartik\date\DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
